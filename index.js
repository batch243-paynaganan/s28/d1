// CRUD Operations
/*
	-crud operations are the heart of any backend application.
	-mastering the crud operations is essential for any developer
	-this helps in building character and increasing exposure
	-mastering the crud operations of any languages amakes us a valuable developer and makes the work easier for us to deal huge amounts of information
*/

// [Section] inserting document (create)
	// insert document
		/*
			since mongoDB deals with objects as its structure for documents, we can easily create them by providing objects into our methods
			mongo shell also uses js for its syntax which makes it convenient for us to understand the code
			syntax:
				db.collectionName.insertOne({object})
			js:
				object.object.method({object});
		*/

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "12345678",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Insert Many
/*
	syntax:
	db.collectionName.insertMany([{ObjectA}, {ObjectB}])
*/

db.users.insertMany([{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact:
	{
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
}, {
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact:{
		phone: "87654321",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}])

// [Section] finding documents (read)
// find
/*
	-if multiple documents match the criteria for finding a documents only the first document that matches the search term will be returned
	-this is based from the order that the documents are stored in a collection
	Syntax:
	db.collectionName.find()
	db.collectionName.find({field: value})
*/

// leaving the search criteria empty will retrieve all the docments
db.users.find();
db.users.find({firstName: "Stephen"});


// pretty method
// pretty method allows us to be able to view documents returned by our terminal in a pretty format
db.users.find({firstName: "Stephen"}).pretty();

// finding documents with multiple parameters
/*
	syntax:
	db.collectionName.find({fieldA: valueA, fieldB: valueB})
*/

db.users.find({lastName: "Armstrong", age: 82}).pretty();

// updating a single document
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact:{
		phone: "00000000",
		email: "test@gmail.com"
	},
	course: [],
	department: "none"
})

/*
just like the find method, update method will manipulate only one document. first document that matches the search criteria will be updated

syntax:
db.colectionName.updateOne({criteria}, {$set:{field: value}})
*/

db.users.updateOne(
	{firstName: "Bill"},{
		$set:{
			lastName: "Gates"
			age: "65",
			contact:{
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	})


// updating multiple documents
/*
	db.collectionName.upateMany({criteria}, {$set{field: value}})
*/

db.users.updateMany(
	{department: "none"}, {
		$set: {
			department: "HR"
		}
	})

// replace one
// can be used if replacing the whole document necessary
/*
syntax:
db.collectionName.replaceOne({criteria},{$set{field: value}})
*/

db.users.replaceOne({firstName: "Bill"},
	{
		firstName: "Mark",
		lastName: "Payns",
		age: 14,
		contact: {
			phone: "12345678",
			email: "mark@gmail.com"
			},
		course: ["PHP", "Laravel", "HTML"],
		department: "Operations"
		}
	)

// [Section] deleting documents (delete)

db.users.insert({
	firstName: "test"
})

// deleting a document
/*
	syntax:
	db.collectionName.deleteOne({criteria});
*/

db.users.deleteOne({firstName: "test"})

// deleteMany
/*
	be careful when using deleteMany method. if no search criteria is provided it will delete all documents in the collection
	DO NOT USE: db.collectionName.deleteMany();
	syntax:
	db.collectionName.deleteMany({criteria})
*/

db.users.deleteMany({firstName: "Mark"})

// [Section] Advanced queries
// query an embedded document

db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
})

// query on nested field

db.users.find({"contact.phone" : "87654321"})

// querying an array with exact elements
db.users.find({courses: ["Python", "React", "PHP"]});

// querying an array without regards to order and elements
db.users.find({courses: {$all : ["React"]}});